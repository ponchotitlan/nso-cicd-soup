# NSO DevOps in a tin can - Gitlab edition: 🇳🇱 Cisco Live Amsterdam 2024 🧇
## CLick to register! 👉🏽 [DEVNET-2224: Embracing DevOps for my NSO Use Cases lifecycle](https://www.ciscolive.com/emea/learn/session-catalog.html?search=devnet-2224#/) 👈🏽

<p align="center">
  <img src="pictures/tincan_orange.png" />
</p>

## Now with more Orange Fox whiskers! 🦊

Gitlab has been recently gaining popularity as the de-facto platform for project development. Its in-built tools allow the handling of the development lifecycle from top to bottom under a single pane of glass. The introduction og Gitlab Runners for DevOps capabilities enhance further the development experience, as it is possible to create, manage and monitor our DevOps pipeline from within the same portal, while also allowing for further integration with the other tools and with just some clicks and lines of code.

This repository aims to describe the setup from start to finish of a NSO-oriented NetDevOps pipeline.

## 🧅 Chopping some onions and NID images

The NSO in Docker (NID) project aims to provide a container-state-of-mind approach to the way in which we maintain staging, development, testing and (why not) production environments.

The holistic container-first approach makes it easy to isolate the purpose of every component, so that each container is dedicated to a specific function only.

The following steps describe the image creation, container spinning, and the different features of each flavor.

First of all, clone the official repository in your host:

```
git clone https://github.com/NSO-developer/nso-docker
```

Download the Cisco NSO free Linux signed.bin image for testing purposes from [this link](https://software.cisco.com/download/home/286331591/type/286283941/release/6.1). The version currently available is v6.1. Once downloaded, issue the following command to extract the installer file:

```
% sh nso-6.1.linux.x86_64.signed.bin . . .
nso-6.1.linux.x86_64.installer.bin
```

This will generate a series of files. Locate the one which ends in ```.installer.bin``` and place it in the ```nso-install-files/``` directory of the Docker for NSO repository.

Once done, issue the following command in the root location of the directory:

```
% make
```

This will compile two different flavors of NSO docker images into your local collection. You can verify the completion with the following command:

```
docker images | grep cisco-nso
cisco-nso-base                                                   6.1-root               a173e5c9baa9   3 months ago    678MB
cisco-nso-dev                                                    6.1-root               825b256d44ac   3 months ago    1.43GB
```

Now, let's prepare a NED skeleton! Navigate to the following location of the ```nso-docker``` repository and copy your unzipped NED folder:

```
/nso-docker/skeletons/ned/packages
```

In our example, we are using the cisco-iosxr-cli-7.43 NED. Now, navigate to the parent ned folder and execute the following command:

```
make build NSO_VERSION=6.1-root
```

What will happen now is that the NED will be compiled and onboarded into our NSO image, so everytime that we spin a new container based on it, we don't need to onboard this NED.

Unfortunately, the compilation is unavoidable, hence this process takes some time ...

Let's have a look at the images generated:

```
docker images | grep ned/
ned/package-cisco-iosxr-cli-7.43                                 6.1-root-alfsando      cecfdbcc8adf   21 minutes ago   53.2MB
ned/package-cisco-iosxr-cli-7.43                                 MM_6.1-root-alfsando   cecfdbcc8adf   21 minutes ago   53.2MB
ned/netsim-cisco-iosxr-cli-7.43                                  6.1-root-alfsando      69591c1be6f1   21 minutes ago   1.48GB
ned/netsim-cisco-iosxr-cli-7.43                                  MM_6.1-root-alfsando   69591c1be6f1   21 minutes ago   1.48GB
ned/netsim                                                       6.1-root-alfsando      69591c1be6f1   21 minutes ago   1.48GB
ned/netsim                                                       MM_6.1-root-alfsando   69591c1be6f1   21 minutes ago   1.48GB
ned/package                                                      6.1-root-alfsando      e3d429421f74   21 minutes ago   53.2MB
ned/package                                                      MM_6.1-root-alfsando   e3d429421f74   21 minutes ago   53.2MB
ned/testnso                                                      6.1-root-alfsando      2e8e4ad543bc   21 minutes ago   732MB
ned/build                                                        6.1-root-alfsando      4089d813c34b   21 minutes ago   1.53GB
```

For this example, we will use the image ```ned/testnso:6.1-root-alfsando``` to spin a NSO container which will automatically boot AND will contain by default our IOSXR NED (pretty cool, hein?), as well as the ```ned/netsim-cisco-iosxr-cli-7.43:6.1-root-alfsando``` image for spinning NETSIM containers based on the same NED.

## 🍜 Warming up the delicious Gitlab Runners

Now it's time to setup and register your Gitlab Runner, which is the client in charge of communicating with your pipeline and executing the actions of the different defined stages. A runner needs to be registered in your Gitlab project, and also it needs to have an executor, which is the actual platform where all these different actions will be performed. 

There are [several types of executors](https://docs.gitlab.com/runner/executors/index.html), but for the purpose of this demo we will use the [Shell](https://docs.gitlab.com/runner/executors/shell.html) one, which consists on hosting all the activities in the local computer running the Gitlab Runner client. The reason for this is that the custom NID images which we created are hosted in our computer. On a further iteration of this demo, these images will be already generated for you and hosted in the Images catalogue of this repository, hence we will be able to explore other executor types such as Docker (a "Docker-in-Docker" fashion).

It is required that you install the Gitlab Runner client in your host computer. The installation is very straightforward. Just follow the steps based on [the OS of your computer](https://docs.gitlab.com/runner/install/) as mentioned in the official website.

Now, navigate to ```Settings > CICD > Runners```. Deactivate the "Shared runners" option, as we will use our local runner instead. These shared runners are available in the Gitlab cloud and provide isolated instances for all your gitlab.com based pipelines.

Next to the "New Project Runner" button, click on the three dots menu. This will display a small pop-up with the Registration Token. Take note of that code.

<p align="center">
  <img src="pictures/gitlab01.png" />
</p>

Open your host OS terminal and input the following commands:

```
% gitlab-runner register

Runtime platform                                    arch=arm64 os=linux pid=20 revision=436955cb version=15.11.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
[http://gitlab]:
***http://gitlab***

Enter the registration token:
***project registration token***

Enter a description for the runner:
*** your description *** 

Enter tags for the runner (comma-separated):
Enter optional maintenance note for the runner:

WARNING: Support for registration tokens and runner parameters in the 'register' command has been deprecated in GitLab Runner 15.6 and will be replaced with support for authentication tokens. For more information, see https://gitlab.com/gitlab-org/gitlab/-/issues/380872 
Registering runner... succeeded                     runner=GR1348941zJ_u_8ku

Enter an executor: parallels, shell, docker+machine, docker-windows, docker, docker-ssh, ssh, virtualbox, docker-autoscaler, docker-ssh+machine, instance, custom, kubernetes:
*** shell ***

Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
 
Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml" 
```

And voilà! The setup is done. You should see your runner registered in your project as follows when refreshing the GitLab website:

<p align="center">
  <img src="pictures/gitlab02.png" />
</p>

The last step is to clone the project to your computer:

```
git clone https://gitlab.com/ponchotitlan/nso-cicd-soup.git
```

Now, everytime that you issue a commit, the pipeline definition will get triggered. It is possibel to track it through the small icon in the main page of the repository:

<p align="center">
  <img src="pictures/gitlab03.png" />
</p>

By clicking there or navigating to ```Build > Pipelines```, it is possible to explore the execution of current and past jobs.

<p align="center">
  <img src="pictures/gitlab04.png" />
</p>

The icon to the right gives you access to the generated artifacts, which include the Robot test cases reports and the compiled ```.tar.gz``` files of your NSO service packages.

<p align="center">
  <img src="pictures/gitlab05.png" />
</p>

Upon clicking on the icon to the left on every pipeline, it is possible to have a look at the different stages and the execution results of each.

<p align="center">
  <img src="pictures/gitlab06.png" />
</p>

Finally, by clicking on each of the stages, it is possible to read the entire log of the execution of the jobs within.

<p align="center">
  <img src="pictures/gitlab07.png" />
</p>

## 🍽️ Serve and enjoy!
The project comes pre-populated with the sample repository nso_awesome_project which consists on the following:

- ```.gitlab-ci.yml```: The pipeline definition per se. Have a look at the different stages and season it to your taste!
- ```packages/router-rfs```: A very simple rfs service package for provisioning access-lists and line templates in IOSXR devices
- ```pipeline_utils/environments.yml```: Variables for defining the docker images and naming in our gitlab pipeline. Update the contents based on your generated resources
- ```pipeline_utils/netsim_authgroup.xml```: Authentication group configuration needed for onboarding any NETSIM device. This is pushed onto our NSO container as part of the pipeline.
- ```pipeline_utils/nso_setup.list```: Configurations for the NSO server which are applied when spinning a new container using the NSO in Docker idiomatic capabilities (enable HTTP, admin user, etc)
- ```tests```: A series of Robot Framework tests for the two servicepoints within the router-rfs  package. The structure consists on input payloads described in .json files which is sent to NSO through REST using the rfs service. Afterwards, the resulting pushed config is verified and matched against the payload in the .txt files.

Feel free to adapt this project to any exciting requirement that you have! And enjoy this bowl of warm soup!

---

Crafted with 🧡  by [Alfonso Sandoval - Cisco](https://linkedin.com/in/asandovalros)