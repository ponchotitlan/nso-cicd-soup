devices device netsim-iosxr-dev--1
 config
  line template test_vty_01
   access-class ingress vty01
   transport input ssh
   transport output telnet
   exec-timeout 10
   session-timeout 5
  exit
 !
!